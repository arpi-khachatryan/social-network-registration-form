# A Social Network Registration Form

## Functional Requirements:


1. The form should include the following fields:
- Username
- Email
- Password
- Date of Birth
- Gender (with options for Male, Female and Other)

2. All fields in the form should be required, meaning users cannot submit the form without filling them out.
3. The email field should only accept valid email addresses.
4. The date of birth field should accept a valid date.
5. If any required field is left empty, an error message should be displayed indicating that all fields are required.
6. If all fields are filled correctly, a success message should be displayed, and the form should be submitted.


## Non-functional Requirements:

1. The HTML should be semantic, with appropriate use of HTML elements.
2. CSS should be stored in external files (styles.css).
3. The form should use JavaScript to prevent default form submission and handle form validation.
4. Alerts should be displayed using JavaScript to provide feedback to the user.
